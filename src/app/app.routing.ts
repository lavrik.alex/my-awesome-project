import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './pages/home/index';
import { ConcoursComponent } from './pages/concours/index';
import { MLComponent } from './pages/mentions-legales/index';


const routes : Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent },
  { path: 'concours', pathMatch: 'full', component: ConcoursComponent  , data: { state: 'concours' } },
  { path: 'mentions-legales', pathMatch: 'full', component: MLComponent  , data: { state: 'mention-legale' } },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)]  ,
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
