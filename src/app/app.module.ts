import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { AppRoutingModule, } from './app.routing';

import { PaletteModule } from './palette/palette.module';
import { HomeComponent } from './pages/home/index';
import { ConcoursComponent } from './pages/concours/index';
import { MLComponent } from './pages/mentions-legales/index';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AutoheightDirective } from './directives/autoheight.directive';

@NgModule({
  declarations: [AppComponent, HomeComponent , ConcoursComponent, MLComponent, AutoheightDirective],
  imports: [
    PaletteModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot() ,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
