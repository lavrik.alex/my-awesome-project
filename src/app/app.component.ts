import { Component } from '@angular/core';
import { routerTransition } from './_animation/router.transition';

@Component({
  selector: 'app-root',
  animations: [routerTransition],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PB100';

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
