import {trigger, animate, style, group, animateChild, query, stagger, transition} from '@angular/animations';

export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    // query(':enter, :leave', style({ opacity: 0 }), { optional: true }),
    // query('.block', style({ opacity: 0 }), {optional: true}),
    // group([
      query(':enter #dynContent', [
        style({ opacity: 0 }),
        animate('1s ease-in-out', style({ opacity: 1 }))
      ], { optional: true }),
      // query(':leave #dynContent', [
      //   style({ opacity: 1 }),
      //   animate('1s ease-in-out', style({ opacity: 0 }))
      // ], { optional: true })
    // ]),
    // query(':enter .block', stagger(400, [
    //   style({ transform: 'translateY(100px)' }),
    //   animate('1s ease-in-out',
    //     style({ transform: 'translateY(0px)', opacity: 1 })),
    // ]), {optional: true}),
  ])
]);
