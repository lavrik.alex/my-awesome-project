import { Directive, ElementRef, HostListener, Input } from '@angular/core';

declare var gapi ;

@Directive({
  selector: '[partage]'
})
export class PartageDirective {

    @Input()
    facebook: FacebookParams;

    @Input()
    twitter : TwitterParams;

    @Input()
    google : GoogleParams;

    @Input()
    pinterest : PinterestParams;

    public sharers : any ;


    constructor(El: ElementRef) {

        this.sharers = {
            facebook: {
                shareUrl: 'https://www.facebook.com/sharer/sharer.php',
            },
            googleplus: {
                shareUrl: 'https://plus.google.com/share',
                isLink: true
            },
            linkedin: {
                shareUrl: 'https://www.linkedin.com/shareArticle',
            },
            twitter: {
                shareUrl: 'https://twitter.com/intent/tweet/',
            },
            email: {
                isLink: true
            },
            whatsapp: {
                shareUrl: 'whatsapp://send',
                isLink: true
            },
            telegram: {
                shareUrl: 'tg://msg_url',
                isLink: true
            },
            viber: {
                shareUrl: 'viber://forward',
                isLink: true
            },
            line: {
                isLink: true
            },
            pinterest: {
                shareUrl: 'https://www.pinterest.com/pin/create/button/',
            },
            tumblr: {
                shareUrl: 'http://tumblr.com/widgets/share/tool',
            },
            hackernews: {
                shareUrl: 'https://news.ycombinator.com/submitlink',
            },
            reddit: {
                shareUrl: 'https://www.reddit.com/submit',
            },
            vk: {
                shareUrl: 'http://vk.com/share.php',
            },
            xing: {
                shareUrl: 'https://www.xing.com/app/user',
            },
            buffer: {
                shareUrl: 'https://buffer.com/add',
            },
            instapaper: {
                shareUrl: 'http://www.instapaper.com/edit',
            },
            pocket: {
                shareUrl: 'https://getpocket.com/save',
            },
            digg: {
                shareUrl: 'http://www.digg.com/submit',
            },
            stumbleupon: {
                shareUrl: 'http://www.stumbleupon.com/submit',
            },
            flipboard: {
                shareUrl: 'https://share.flipboard.com/bookmarklet/popout',
            },
            renren: {
                shareUrl: 'http://share.renren.com/share/buttonshare',
            },
            myspace: {
                shareUrl: 'https://myspace.com/post',
            },
            blogger: {
                shareUrl: 'https://www.blogger.com/blog-this.g',
            },
            baidu: {
                shareUrl: 'http://cang.baidu.com/do/add',
            },
            douban: {
                shareUrl: 'https://www.douban.com/share/service',
            },
            okru: {
                shareUrl: 'https://connect.ok.ru/dk',
            }
        };
    
    }

    @HostListener('click') OnClick() {
        let sharer = this.getSharer();
        var p = sharer.params || {}, keys = Object.keys(p), i, str = keys.length > 0 ? '?' : '';
        for (i = 0; i < keys.length; i++) {
            if (str !== '?') {
                str += '&';
            }
            if (p[keys[i]]) {
                str += keys[i] + '=' + encodeURIComponent(p[keys[i]]);
            }
        }
        sharer.shareUrl += str;

        if (!sharer.isLink) {
            var popWidth = sharer.width || 600, popHeight = sharer.height || 480, left = window.innerWidth / 2 - popWidth / 2 + window.screenX, top = window.innerHeight / 2 - popHeight / 2 + window.screenY, popParams = 'scrollbars=no, width=' + popWidth + ', height=' + popHeight + ', top=' + top + ', left=' + left, newWindow = window.open(sharer.shareUrl, '', popParams);
            if (window.focus) {
                newWindow.focus();
            }
        }else {
            window.location.href = sharer.shareUrl;
        }
    }

    getSharer() {
        let _sharer;
        if (this.facebook) {
            _sharer = this.sharers['facebook'];
            _sharer.params = this.facebook;
        }
        if (this.google) {
            _sharer = this.sharers['googleplus'];
            _sharer.params = this.google;
        }
        if (this.twitter) {
            _sharer = this.sharers['twitter'];
            _sharer.params = this.twitter;
        }
        if (this.pinterest) {
            _sharer = this.sharers['pinterest'];
            _sharer.params = this.pinterest;
        }

        return _sharer;
    };
    
}

class FacebookParams {
    public url : string ;
}
class GoogleParams {
    public url : string ;
}
class TwitterParams {
    text: string;
    url: string;
    hashtags: string;
    via: string;
}
class PinterestParams {
    url: string;
    media: string;
    description: string;
}

