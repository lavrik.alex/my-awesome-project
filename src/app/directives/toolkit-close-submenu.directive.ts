import { Directive, ElementRef, HostListener } from '@angular/core';



@Directive({
  selector: '[appToolkitCloseSubmenu]'
})
export class ToolkitCloseSubmenuDirective {

  constructor(El: ElementRef) {}

  @HostListener('click') OnClick() {
    $('.subTool').removeClass('opened');
    $('#mainTool').removeClass('closed');
    $('.toolAction a.close').removeClass('show');
  }

}
