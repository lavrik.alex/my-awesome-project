import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appToolkitOpenSubmenu]'
})
export class ToolkitOpenSubmenuDirective {

  @Input() 
  subMenuTargetID: string;
  
  constructor() {}

  @HostListener('click') OnClick() {
    if ($("#" + this.subMenuTargetID).length) {
      $('#' + this.subMenuTargetID).addClass('opened');
      $('#mainTool').addClass('closed');
      $('.subTool').not('#' + this.subMenuTargetID).removeClass('opened');
      $('.toolAction a.close').addClass('show');
    }
  }

}
