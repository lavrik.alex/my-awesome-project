import { Directive, ElementRef } from '@angular/core';



@Directive({
  selector: '[appMenutoggle]'
})
export class MenutoggleDirective {
  constructor(El: ElementRef) {
    $(El.nativeElement).on('click', function(e) {
      e.preventDefault();
      $('body').toggleClass('menuOpened');
    });
  }
}
