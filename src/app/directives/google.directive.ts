import { Directive, ElementRef , Input , AfterViewInit } from '@angular/core';

declare var gapi : any;

@Directive({
  selector: '[google]'
})
export class GoogleDirective implements AfterViewInit {

    @Input()
    google : any ;

    public element : ElementRef;

    constructor(El: ElementRef) {
        this.element = El;
    }

    ngAfterViewInit(){
        var options = {
            contenturl: this.google.url,
            clientid: '481793538430-n7v040p7ptf3k1n6m1e2e2c89fev8ldb.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            prefilltext: this.google.quote,
            calltoactionlabel: 'JOIN',
            calltoactionurl: 'https://fr.petitbateau.nouvoduo.fr'
        };
        // Call the render method when appropriate within your app to display
        gapi.interactivepost.render(this.element.nativeElement, options); 
    }


}
