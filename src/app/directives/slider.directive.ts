import { Directive, ElementRef } from '@angular/core';



@Directive({
  selector: '[appPbSlider]'
})
export class SliderDirective {
  constructor(el: ElementRef) {
    setTimeout(() => {
      (<any>$(el.nativeElement))
        .on('init', function(event, slick, direction) {
          if($(window).width() > 767 && $(window).width() < 1200)
            (<any>$('.slide-box')).matchHeight();
          else
            (<any>$('.slide-box, .histoireBloc > div.image')).matchHeight();

          (<any>$('#homeSlide .slider')).animate({opacity: 1});
        })
        .slick({
          dots: true,
          adaptiveHeight: $(window).width() < 768 ? true : false
        });
    },500);
  }
}
