import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appAutoheight]'
})
export class AutoheightDirective {

  constructor(El: ElementRef) {
  	var mainContentHeight = $('#mainContent').height();
  	var topSectionHeight = $('#topSection').height() + ($('#logo').height() - $('#topSection').height());
  	console.log(mainContentHeight);
  	console.log(topSectionHeight);
  	$(El.nativeElement).height(mainContentHeight - topSectionHeight).addClass('show');
  }

}
