import { Directive, ElementRef , Input , HostListener } from '@angular/core';

declare var FB : any;

@Directive({
  selector: '[facebook]'
})
export class FacebookDirective {

    @Input()
    facebook : any ;

    constructor(El: ElementRef) {


    }

    @HostListener('click') OnClick() {
        FB.ui({
            method: 'share',
            href: this.facebook.url,
            quote : this.facebook.quote
        }, function(response){});
    }

}
