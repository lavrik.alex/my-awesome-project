import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appGotostep]'
})
export class GotostepDirective {

  @Input() step: string;
  constructor(El: ElementRef) {
    
  }

  @HostListener('click') OnClick() {
    console.log('step ' + this.step + '=' + $('[data-step="' + this.step + '"]').length);
    if ($('[data-step="' + this.step + '"]').length) {
      //$('.step.show').removeClass('show');
      //$('.step[data-step="' + this.step + '"]').addClass('show');
    }
  }

}
