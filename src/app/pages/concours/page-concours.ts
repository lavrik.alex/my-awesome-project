import { Component } from '@angular/core';
import { fadeInAnimation } from './../../_animation/index';

@Component({
  templateUrl: 'page-concours.html',
  animations: [fadeInAnimation],
  host: { '[@fadeIn]': '' }
})
export class ConcoursComponent {}
