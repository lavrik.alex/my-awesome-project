import { Component } from '@angular/core';
import { fadeInAnimation } from './../../_animation/index';

@Component({
  templateUrl: 'page-home.html',
  animations: [fadeInAnimation],
  host: { '[@fadeIn]': '' }
})
export class HomeComponent {

}
