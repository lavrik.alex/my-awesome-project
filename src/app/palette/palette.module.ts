import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgClass } from '@angular/common';
import { HttpModule, JsonpModule , Http } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { MenutoggleDirective } from './../directives/menutoggle.directive';
import { ToolkitOpenSubmenuDirective } from './../directives/toolkit-open-submenu.directive';
import { ToolkitCloseSubmenuDirective } from './../directives/toolkit-close-submenu.directive';
import { SliderDirective } from './../directives/slider.directive';
import { GotostepDirective } from './../directives/gotostep.directive';
import { PartageDirective } from './../directives/partage.directive';
import { GoogleDirective } from './../directives/google.directive';
import { FacebookDirective } from './../directives/facebook.directive';

@NgModule({
  imports: [
    CommonModule ,
    FormsModule ,
    HttpModule ,
    JsonpModule ,
    ReactiveFormsModule,
    NgbModule 
  ],
  declarations: [ 
    ToolkitOpenSubmenuDirective, 
    ToolkitCloseSubmenuDirective , 
    SliderDirective ,
     GotostepDirective , 
     MenutoggleDirective , 
     PartageDirective ,
     GoogleDirective ,
     FacebookDirective
  ],
  entryComponents : [
    
  ],
  exports : [
      ToolkitOpenSubmenuDirective, 
      ToolkitCloseSubmenuDirective , 
      SliderDirective , 
      GotostepDirective , 
      MenutoggleDirective , 
      PartageDirective ,
      GoogleDirective ,
      FacebookDirective
  ]
})

export class PaletteModule {}