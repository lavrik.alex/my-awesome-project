- Compilation sous une langue :
  ng build --aot --i18nFile=src/locale/messages.en.xlf --i18nFormat=xlf --locale=en

- creation de fichier langue :
  ng xi18n --outputPath src/locale --locale fr

test